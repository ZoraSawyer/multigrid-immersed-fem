#! /usr/bin/env python3

import functionLibrary
import numpy, scipy, pickle, time

def load_dataFile(path):
  with open(path, 'rb') as fileName:
    return pickle.load(fileName)
def dump_dataFile(path,files):
  with open(path, 'wb') as fileName:
    return pickle.dump(files,fileName)

# set parameters
degree = 2
nElems = 16
levels= 4
maxiter= 100 # maximum number of iterations
invertTol = 1e-16 # treshold for eliminating basis functions (see Remark 3.3)

# list of elements intersecting the physical domain
activeElems = load_dataFile('Matrices/Tooth_Matrices_nElems_'+str(int(nElems))+'_degree_'+str(int(degree))+'_elems' )

# system matrix
n = 3*(degree*nElems+1)**3
systemMatrixData    = numpy.load( 'Matrices/Tooth_Matrices_nElems_'+str(int(nElems))+'_degree_'+str(int(degree))+'_data.npy'    )
systemMatrixIndices = numpy.load( 'Matrices/Tooth_Matrices_nElems_'+str(int(nElems))+'_degree_'+str(int(degree))+'_indices.npy' )
systemMatrixIndptr  = numpy.load( 'Matrices/Tooth_Matrices_nElems_'+str(int(nElems))+'_degree_'+str(int(degree))+'_indptr.npy'  )
systemMatrix = scipy.sparse.csr_matrix(( systemMatrixData, systemMatrixIndices, systemMatrixIndptr), shape=(n, n))

# right hand side
rhs = numpy.load( 'Matrices/Tooth_Matrices_nElems_'+str(int(nElems))+'_degree_'+str(int(degree))+'_rhs.npy'  )

# set up preconditioning routine
startTime = time.clock()
systemMatrixList, restrictionProlongationmatrixList, blockListList, blockInverseListList = functionLibrary.multigridInit( systemMatrix, nElems, nElems, nElems, activeElems, degree, levels, invertTol )
elapsedSetupTime = time.clock()-startTime

# solve system
startTime = time.clock()
sol, residualV = functionLibrary.preconCG( rhs, 1e-10, int(maxiter), systemMatrixList, restrictionProlongationmatrixList, blockListList, blockInverseListList, 'multiplicative', [False], [True], 1 )
elapsedSolveTime = time.clock()-startTime
print('elapsed time setup =',elapsedSetupTime)
print('elapsed time solve =',elapsedSolveTime)
