import numpy, scipy.linalg, scipy.sparse, scipy.sparse.linalg

# function to create the restriction/prolongation matrices
# active is a boolean array denoting if a function is supported (see Remark 3.3)
#
def setRestrictionProlongationmatrix( zNElemsFine, yNElemsFine, xNElemsFine, degree, active ):

  # number of functions and elements per dimension
  #
  zNFuncsFine = int(zNElemsFine+degree)
  zNElemsCoarse = int(zNElemsFine/2)
  zNFuncsCoarse = int(zNElemsCoarse+degree)
  yNFuncsFine = int(yNElemsFine+degree)
  yNElemsCoarse = int(yNElemsFine/2)
  yNFuncsCoarse = int(yNElemsCoarse+degree)
  xNFuncsFine = int(xNElemsFine+degree)
  xNElemsCoarse = int(xNElemsFine/2)
  xNFuncsCoarse = int(xNElemsCoarse+degree)

  # create empty matrix
  #
  RestrictionProlongationmatrix = scipy.sparse.lil_matrix(( 3*zNFuncsCoarse*yNFuncsCoarse*xNFuncsCoarse, 3*zNFuncsFine*yNFuncsFine*xNFuncsFine ))

  # transformation coefficients
  #
  if degree == 1:
    factors1st = [1  , 1/2]
    factorsStd = [1/2, 1  , 1/2]
  if degree == 2:
    factors1st = [1  , 1/2]
    factors2nd = [1/2, 3/4, 1/4]
    factorsStd = [1/4, 3/4, 3/4, 1/4]
  if degree == 3:
    factors1st = [1  , 1/2  ]
    factors2nd = [1/2, 3/4  , 3/16]
    factors3rd = [1/4, 11/16, 1/2 , 1/8]
    factorsStd = [1/8, 1/2  , 3/4 , 1/2, 1/8]

  # loop over functions
  # the 'Covered' variable contains the indices of fine functions that construct the coarse function
  #
  for zFuncCoarse in numpy.arange(zNFuncsCoarse):
    if zFuncCoarse < degree: # functions with repeated knots at start knot vector
      if zFuncCoarse == 0:
        zFactors = numpy.reshape( factors1st, (-1, 1, 1) ) # three-dimensional object for tensor product multiplication 
      elif zFuncCoarse == 1:
        zFactors = numpy.reshape( factors2nd, (-1, 1, 1) )
      elif zFuncCoarse == 2:
        zFactors = numpy.reshape( factors3rd, (-1, 1, 1) )
      zCovered = numpy.reshape( numpy.arange( zFuncCoarse, 2*(zFuncCoarse+1) ), (-1, 1, 1) )
    elif zFuncCoarse > zNFuncsCoarse-degree-1: # functions with repeated knots at end knot vector   
      if zFuncCoarse == zNFuncsCoarse-1-0:
        zFactors = numpy.reshape( numpy.flipud(factors1st), (-1, 1, 1) )
      elif zFuncCoarse == zNFuncsCoarse-1-1:
        zFactors = numpy.reshape( numpy.flipud(factors2nd), (-1, 1, 1) )
      elif zFuncCoarse == zNFuncsCoarse-1-2:
        zFactors = numpy.reshape( numpy.flipud(factors3rd), (-1, 1, 1) )
      zCovered = zNFuncsFine-1 - numpy.reshape( numpy.flipud(numpy.arange( (zNFuncsCoarse-1-zFuncCoarse), 2*((zNFuncsCoarse-1-zFuncCoarse)+1) )), (-1, 1, 1) )
    else: # functions without repeated knots
      zFactors = numpy.reshape( factorsStd, (-1, 1, 1) )
      zCovered = degree + 2*(zFuncCoarse-degree) + numpy.reshape( numpy.arange( degree+2 ), (-1, 1, 1) )
    for yFuncCoarse in numpy.arange(yNFuncsCoarse):
      if yFuncCoarse < degree:
        if yFuncCoarse == 0:
          yFactors = numpy.reshape( factors1st, (1, -1, 1) )
        elif yFuncCoarse == 1:
          yFactors = numpy.reshape( factors2nd, (1, -1, 1) )
        elif yFuncCoarse == 2:
          yFactors = numpy.reshape( factors3rd, (1, -1, 1) )
        yCovered = numpy.reshape( numpy.arange( yFuncCoarse, 2*(yFuncCoarse+1) ), (1, -1, 1) )
      elif yFuncCoarse > yNFuncsCoarse-degree-1:                                                
        if yFuncCoarse == yNFuncsCoarse-1-0:
          yFactors = numpy.reshape( numpy.flipud(factors1st), (1, -1, 1) )
        elif yFuncCoarse == yNFuncsCoarse-1-1:
          yFactors = numpy.reshape( numpy.flipud(factors2nd), (1, -1, 1) )
        elif yFuncCoarse == yNFuncsCoarse-1-2:
          yFactors = numpy.reshape( numpy.flipud(factors3rd), (1, -1, 1) )
        yCovered = yNFuncsFine-1 - numpy.reshape( numpy.flipud(numpy.arange( (yNFuncsCoarse-1-yFuncCoarse), 2*((yNFuncsCoarse-1-yFuncCoarse)+1) )), (1, -1, 1) )
      else:
        yFactors = numpy.reshape( factorsStd, (1, -1, 1) )
        yCovered = degree + 2*(yFuncCoarse-degree) + numpy.reshape( numpy.arange( degree+2 ), (1, -1, 1) )
      for xFuncCoarse in numpy.arange(xNFuncsCoarse):
        if xFuncCoarse < degree:
          if xFuncCoarse == 0:
            xFactors = numpy.reshape( factors1st, (1, 1, -1) )
          elif xFuncCoarse == 1:
            xFactors = numpy.reshape( factors2nd, (1, 1, -1) )
          elif xFuncCoarse == 2:
            xFactors = numpy.reshape( factors3rd, (1, 1, -1) )
          xCovered = numpy.reshape( numpy.arange( xFuncCoarse, 2*(xFuncCoarse+1) ), (1, 1, -1) )
        elif xFuncCoarse > xNFuncsCoarse-degree-1:                                                
          if xFuncCoarse == xNFuncsCoarse-1-0:
            xFactors = numpy.reshape( numpy.flipud(factors1st), (1, 1, -1) )
          elif xFuncCoarse == xNFuncsCoarse-1-1:
            xFactors = numpy.reshape( numpy.flipud(factors2nd), (1, 1, -1) )
          elif xFuncCoarse == xNFuncsCoarse-1-2:
            xFactors = numpy.reshape( numpy.flipud(factors3rd), (1, 1, -1) )
          xCovered = xNFuncsFine-1 - numpy.reshape( numpy.flipud(numpy.arange( (xNFuncsCoarse-1-xFuncCoarse), 2*((xNFuncsCoarse-1-xFuncCoarse)+1) )), (1, 1, -1) )
        else:
          xFactors = numpy.reshape( factorsStd, (1, 1, -1) )
          xCovered = degree + 2*(xFuncCoarse-degree) + numpy.reshape( numpy.arange( degree+2 ), (1, 1, -1) )

        # index of coarse function and by tensor product indices and coefficients of fine functions
        # values added to matrix for basis functions describing x, y, and z displacements
        # only fine basis functions that are active are considered
        #
        CoarseIndex = zFuncCoarse + zNFuncsCoarse*yFuncCoarse + zNFuncsCoarse*yNFuncsCoarse*xFuncCoarse
        FineIndices = numpy.ravel( zCovered + zNFuncsFine*yCovered + zNFuncsFine*yNFuncsFine*xCovered)
        Factors = numpy.ravel( zFactors*yFactors*xFactors )
        RestrictionProlongationmatrix[CoarseIndex,FineIndices[active[FineIndices]]] = Factors[active[FineIndices]]
        CoarseIndex += zNFuncsCoarse*yNFuncsCoarse*xNFuncsCoarse
        FineIndices += zNFuncsFine*yNFuncsFine*xNFuncsFine
        RestrictionProlongationmatrix[CoarseIndex,FineIndices[active[FineIndices]]] = Factors[active[FineIndices]]
        CoarseIndex += zNFuncsCoarse*yNFuncsCoarse*xNFuncsCoarse
        FineIndices += zNFuncsFine*yNFuncsFine*xNFuncsFine
        RestrictionProlongationmatrix[CoarseIndex,FineIndices[active[FineIndices]]] = Factors[active[FineIndices]]

  return RestrictionProlongationmatrix

# function to determine which coarse elements are supported 
#
def supportCoarsening( activeElemsFine, zNElemsFine, yNElemsFine, xNElemsFine ):

  # number of coarse elements per dimension
  #
  zNElemsCoarse = int(zNElemsFine/2)
  yNElemsCoarse = int(yNElemsFine/2)
  xNElemsCoarse = int(xNElemsFine/2)

  activeElemsCoarse = set()

  # loop over supported fine elements
  #
  for activeElemFine in activeElemsFine:
    # get indices per dimension
    xElemFine = numpy.floor_divide( activeElemFine, zNElemsFine*yNElemsFine )
    yElemFine = numpy.floor_divide( numpy.remainder( activeElemFine, zNElemsFine*yNElemsFine ), zNElemsFine )
    zElemFine = numpy.remainder( activeElemFine, zNElemsFine )

    # determine intersecting coarse element
    #
    zElemCoarse = numpy.floor_divide(zElemFine,2)
    yElemCoarse = numpy.floor_divide(yElemFine,2)
    xElemCoarse = numpy.floor_divide(xElemFine,2)

    # add to set
    #
    activeElemsCoarse.add( zElemCoarse + zNElemsCoarse*yElemCoarse + zNElemsCoarse*yNElemsCoarse*xElemCoarse )

  return activeElemsCoarse

# function to create a dictionary of elements containing the set of functions supported per element
# and a dictionary of functions containing the set of elements the function is suppored on
#
def setDictionary( activeElems, zNElems, yNElems, xNElems, degree ):

  # number of functions per dimension
  #
  zNFuncs = zNElems+degree
  yNFuncs = yNElems+degree
  xNFuncs = xNElems+degree

  # initiate dictionaries
  #
  elemDict = {}
  funcDict = {}
 
  # loop over elements intersecting the physical domain
  #
  for elem in activeElems:
    # obtain element indices in x, y, and z dimension
    #
    xElem = numpy.floor_divide( elem, zNElems*yNElems )
    yElem = numpy.floor_divide( numpy.remainder( elem, zNElems*yNElems ), zNElems )
    zElem = numpy.remainder( elem, zNElems )

    # indices in x, y, and z dimension of the functions supported on this element
    #
    zFuncs = zElem + numpy.reshape( numpy.arange( degree+1 ), (-1, 1, 1) ) # three-dimensional object for tensor product multiplication
    yFuncs = yElem + numpy.reshape( numpy.arange( degree+1 ), (1, -1, 1) )
    xFuncs = xElem + numpy.reshape( numpy.arange( degree+1 ), (1, 1, -1) )

    # create item in the element dictionary with functions supported on it
    #
    elemDict[elem] = set( numpy.ravel( zFuncs + zNFuncs*yFuncs + zNFuncs*yNFuncs*xFuncs ) )
    
    # loop over functions supported on element
    #
    for func in numpy.ravel( zFuncs + zNFuncs*yFuncs + zNFuncs*yNFuncs*xFuncs ):
      if func in funcDict:
        # add element to the set of elements on which the function is supported
        #
        funcDict[func].add( elem )
      else:
        # create item for function and
        # add element to the set of elements on which the function is supported
        #
        funcDict[func] = set([ elem ])

  return elemDict, funcDict

# function to compute a list of blocks for multiplicative Schwarz
# 
def setBlockList( elemDict, funcDict, zNElems, yNElems, xNElems, degree ):

  colorScheme = degree+1 # number of colors per dimension for scalar problem
  zNFuncs = zNElems+degree
  yNFuncs = yNElems+degree
  xNFuncs = xNElems+degree

  # initiate list
  blockList  = []
  # create list for every color
  for blockNumber in numpy.arange(3*colorScheme**3):
    blockList.append( [] )

  # loop over functions
  for func in funcDict:
    # elements the function is supported on
    elems = funcDict[func]
    # candidates are functions that intersect (i.e. are supported on one of the elements)
    candidates = set()
    for elem in elems:
      candidates.update(elemDict[elem])

    # set with functions in the block
    blockSet = set()
    # loop over candidates
    for candidate in candidates:
      # if function is fully inside the support
      if funcDict[candidate].issubset(funcDict[func]):
        blockSet.add(candidate)

    # routine to obtain color of the block by dimension-wise indices of the function
    # 
    xIndex = numpy.floor_divide( func, zNFuncs*yNFuncs )
    yIndex = numpy.floor_divide( numpy.remainder( func, zNFuncs*yNFuncs ), zNFuncs )
    zIndex = numpy.remainder( func, zNFuncs )
    color = numpy.remainder( zIndex, colorScheme ) + colorScheme*numpy.remainder( yIndex, colorScheme ) + colorScheme**2*numpy.remainder( xIndex, colorScheme )

    # create blocks describing the displacement in all dimensions
    #
    blockList[color                 ].append( numpy.array(list(blockSet)) )
    blockList[color+  colorScheme**3].append( numpy.array(list(blockSet))+  zNFuncs*yNFuncs*xNFuncs )
    blockList[color+2*colorScheme**3].append( numpy.array(list(blockSet))+2*zNFuncs*yNFuncs*xNFuncs )

  return blockList

def sliceMatrix( systemMatrix, block ):
  blockMatrix = numpy.zeros([len(block), len(block)]) # initate empty block matrix
  for localRow, globalRow in enumerate(block): # loop over rows
    beg = systemMatrix.indptr[globalRow]
    end = systemMatrix.indptr[globalRow+1]
    if end>beg: # if row of systemMatrix not empty (otherwise result is zeros)
      absCols = systemMatrix.indices[beg:end] # absolute column indices in row
      relCols = numpy.searchsorted( absCols, block ) # relative indices of block with respect to absCols
      relCols[relCols==len(absCols)] = 0 # searchsorted returns len(absCols) (out of range) for block indices larger than largest absolute column index in absCols
      found = block==absCols[relCols] # only use the "found" indices
      blockMatrix[localRow, found] = systemMatrix.data[beg+relCols[found]] 
  return blockMatrix

# function to identify unstable functions
#
def eliminate( systemMatrix, blockList, invertTol ):

  # set tresHold for local eigenvalues
  tresHold = invertTol*numpy.amax(systemMatrix.diagonal())

  # initiate vector describing if functions are active
  active = systemMatrix.diagonal()>tresHold

  # loop over colors
  for color in numpy.arange( len(blockList) ):
    print( 'stabilizing color', color, 'of', len(blockList) )

    # loop over blocks
    for blockN in numpy.arange( len(blockList[color]) ):
      block = blockList[color][blockN]
      if len(block)>1:
        # obtain block matrix and current status of the basis functions
        blockMatrix = sliceMatrix( systemMatrix, block )
        blockActive = active[block]
        # eliminate basis functions until the block is stable
        while numpy.any(blockActive):
          # get local eigenvalues
          localVals,localVecs = numpy.linalg.eigh( blockMatrix[numpy.ix_(blockActive,blockActive)] )
          if numpy.amin(localVals) < tresHold: # if unstable eliminate basis function
            smallMode = numpy.argmin( localVals ) # local index of smallest local eigenvalue
            smallVec = numpy.ravel(localVecs[:,smallMode]) # local eigenvector
            localUnstableIndex = numpy.argmax( numpy.abs(smallVec) ) # dominant function
            active[block[blockActive][localUnstableIndex]] = False # eliminate
            blockActive = active[block]
          else: # if stable break out of while loop
            break

  return active

# function to remove the unstable functions from the blocks (see Remark 3.3)
# and create a list with inverses of the blocks
#
def constructBlockInverseList( fullBlockList, systemMatrix, active ):

  # initiate lists
  activeBlockList = []
  blockInverseList = []

  # loop over colors
  for color in numpy.arange( len(fullBlockList) ):
    print( 'inverting color', color, 'of', len(fullBlockList) )
    activeBlockList.append( [] )
    blockInverseList.append( [] )

    # loop over blocks
    for blockN in numpy.arange( len(fullBlockList[color]) ):

      # invert and append to list
      block = fullBlockList[color][blockN]
      activeBlock = block[active[block]]
      if len(activeBlock)>0:
        activeBlockList[color].append( activeBlock )
        blockInverseList[color].append( scipy.linalg.inv( sliceMatrix( systemMatrix, activeBlock ) ) )

  return activeBlockList, blockInverseList

# function to perform a smoothing sequence
#
# scheme is either 'additive' or multiplicative to determine if additive of multiplicative Schwarz is performed
# forward is a list of booleans determining the smoothing direction (i.e. len(forward) is the number of smoothing steps, True is a forward sweep, False is a backward sweep)
# relax is the relaxation parameter
#
def smooth( systemMatrix, rhs, blockList, blockInverseList, scheme, forward, relax ):

  # initiate lhs and current residual r
  lhs = numpy.zeros( rhs.shape )
  r = rhs

  # forward indicates if a forward or a backward sweep is performed
  # create a vector of colors to iterate over
  if forward:
    colorV =               numpy.arange(len(blockList))
  else:
    colorV = numpy.flipud( numpy.arange(len(blockList)) )

  # loop over colors
  for color in colorV:
 
    # initate lhs vector for the color 
    lhsColor = numpy.zeros( lhs.shape )
  
    # forward indicates if a forward or a backward sweep is performed
    # create a vector of blocks to iterate over
    if forward:
      blockV =               numpy.arange(len(blockList[color]))
    else:
      blockV = numpy.flipud( numpy.arange(len(blockList[color])) )

    # loop over the blocks
    for blockN in blockV:
 
      # update the solution 
      lhsColor[blockList[color][blockN]] += blockInverseList[color][blockN].dot( r[blockList[color][blockN]] )
 
    # add to total solution and update residual in case of multiplicative Schwarz 
    if scheme == 'multiplicative':
      lhs += lhsColor
      r = rhs - systemMatrix.dot(lhs)
    elif scheme == 'additive':
      lhs += relax*lhsColor

  return lhs

# function to prepare the multigrid cycle
#
# activeElems is a set of elements that intersect the pysical domain
# invertTol is a (small) parameter to set a treshold for eliminating basis functions with contributions of the order of the machine precision from the preconditioner (see Remark 3.3)
#
def multigridInit( systemMatrix, zNElems, yNElems, xNElems, activeElems, degree, levels, invertTol ):

  ## level = levels
  systemMatrixList = [ systemMatrix ] # list with matrices at the different levels
  elemDict, funcDict = setDictionary( activeElems, zNElems, yNElems, xNElems, degree ) # dictionaries to construct blocks
  fullBlockList = setBlockList( elemDict, funcDict, zNElems, yNElems, xNElems, degree ) # lists of Schwarzz blocks
  active = eliminate( systemMatrixList[-1], fullBlockList, invertTol ) # array with status of basis functions (functions can be eliminated in the preconditioner for stability, see Remark 3.3)
  activeBlockList, blockInverseList = constructBlockInverseList( fullBlockList, systemMatrixList[-1], active ) # active blocks and inverses
  blockListList = [ activeBlockList ] # create list of lists with blocks at different levels
  blockInverseListList = [ blockInverseList ] # create list of lists with inverses of blocks at different levels

  ## level = 2 -- levels-1
  restrictionProlongationmatrixList = [] # list with restriction/prolongation matrices
  for level in numpy.arange(1,levels-1): # loop over the levels
    if numpy.remainder( zNElems, 2 ) != 0 or numpy.remainder( yNElems, 2 ) != 0 or numpy.remainder( xNElems, 2 ) != 0:
      print('warning: number of elements not divisible by 2')
      raise error
    restrictionProlongationmatrixList.append( setRestrictionProlongationmatrix( zNElems, yNElems, xNElems, degree, active ) )
    reducedMatrix = scipy.sparse.csr_matrix( restrictionProlongationmatrixList[-1].dot( systemMatrixList[-1].dot( restrictionProlongationmatrixList[-1].T )))
    # creating csr matrices in this manner does not always yield monotonically increasing column indices
    # this is fixed in the next steps
    shape = reducedMatrix.shape
    Indptr = reducedMatrix.indptr
    Indices = reducedMatrix.indices
    Data = reducedMatrix.data
    for row in numpy.arange( len(Indptr)-1 ):
      beg = Indptr[row]
      end = Indptr[row+1]
      order = numpy.argsort( Indices[beg:end] )
      Indices[beg:end] = Indices[beg:end][order]
      Data[beg:end] = Data[beg:end][order]
    systemMatrixList.append( scipy.sparse.csr_matrix(( Data, Indices, Indptr), shape=shape ) )
    activeElems = supportCoarsening( activeElems, zNElems, yNElems, xNElems ) # create list of active elements at the coarser level
    zNElems = int(zNElems/2)
    yNElems = int(yNElems/2)
    xNElems = int(xNElems/2)
    elemDict, funcDict = setDictionary( activeElems, zNElems, yNElems, xNElems, degree )
    fullBlockList = setBlockList( elemDict, funcDict, zNElems, yNElems, xNElems, degree )
    active  = eliminate( systemMatrixList[-1], fullBlockList, invertTol )
    activeBlockList, blockInverseList = constructBlockInverseList( fullBlockList, systemMatrixList[-1], active )
    blockListList.append( activeBlockList )
    blockInverseListList.append( blockInverseList )

  ## level = 1
  if levels > 1:
    if numpy.remainder( zNElems, 2 ) != 0 or numpy.remainder( yNElems, 2 ) != 0 or numpy.remainder( xNElems, 2 ) != 0:
      print('warning: number of elements not divisible by 2')
      raise error
    restrictionProlongationmatrix = setRestrictionProlongationmatrix( zNElems, yNElems, xNElems, degree, active )
    reducedMatrix = scipy.sparse.csr_matrix( restrictionProlongationmatrix.dot( systemMatrixList[-1].dot( restrictionProlongationmatrix.T )))
    # at this level a direct solve is performed, but still the blocks are created first to eliminate unstable basis functions
    shape = reducedMatrix.shape
    Indptr = reducedMatrix.indptr
    Indices = reducedMatrix.indices
    Data = reducedMatrix.data
    for row in numpy.arange( len(Indptr)-1 ):
      beg = Indptr[row]
      end = Indptr[row+1]
      order = numpy.argsort( Indices[beg:end] )
      Indices[beg:end] = Indices[beg:end][order]
      Data[beg:end] = Data[beg:end][order]
    reducedMatrix = scipy.sparse.csr_matrix(( Data, Indices, Indptr), shape=shape )
    activeElems = supportCoarsening( activeElems, zNElems, yNElems, xNElems )
    zNElems = int(zNElems/2)
    yNElems = int(yNElems/2)
    xNElems = int(xNElems/2)
    elemDict, funcDict = setDictionary( activeElems, zNElems, yNElems, xNElems, degree )
    fullBlockList = setBlockList( elemDict, funcDict, zNElems, yNElems, xNElems, degree )
    active = eliminate( reducedMatrix, fullBlockList, invertTol ) 
    restrictionProlongationmatrixList.append( restrictionProlongationmatrix[active,:] ) # this restriction matrix only contains the active functions on the coarsest level
    systemMatrixList.append( scipy.sparse.csr_matrix( restrictionProlongationmatrixList[-1].dot( systemMatrixList[-1].dot( restrictionProlongationmatrixList[-1].T ))) )

  return systemMatrixList, restrictionProlongationmatrixList, blockListList, blockInverseListList

# this function performs the V-cycle
# 
# systemMatrixList is a list with system matrices at the different levels
# rhs is the right hand side
# restrictionProlongationMatrixList is a list of restriction/prolongation matrices
# blockListList is a list with blockListst at the different levels
# blockInverseListList is a list with lists of the inverses of the blocks at the different levels
# scheme is either 'additive' or 'multiplicative' and determines if additive Schwarz or multiplicative Schwarz is performed (see the function smooth(...))
# preSmooth is a boolean array, the lenth of the array is the number of presmoothing steps, True is a forward sweep, False is a backward sweep (only relevant for multiplicative Schwarz)
# postSmooth is similar to preSmooth
# relax is the relaxation parameter (only relevant for additive Schwarz)
#
def VCycle( systemMatrixList, rhs, restrictionProlongationmatrixList, blockListList, blockInverseListList, scheme, preSmooth, postSmooth, relax ):

  # perform the VCycle only when more than one level is used
  if len(systemMatrixList) > 1:
    # lists with residuals and approximations of the solution at the different levels
    rhsList = [rhs]
    lhsList = []
  
    # loop over the levels
    for level in numpy.arange(len(systemMatrixList)-1):
      # initiate a zero vector to the list of approximations
      lhsList.append( numpy.zeros(rhsList[level].shape) )
      # perform the presmoothing operations
      for forward in preSmooth:
        lhsList[level] += smooth( systemMatrixList[level], rhsList[level]-systemMatrixList[level].dot(lhsList[level]), blockListList[level], blockInverseListList[level], scheme, forward, relax )
      #compute residual at coarser level
      rhsList.append( restrictionProlongationmatrixList[level].dot( rhsList[level] - systemMatrixList[level].dot(lhsList[level]) ) )

    # apply direct solve at coarsest level
    lhsList.append( scipy.sparse.linalg.spsolve(systemMatrixList[-1], rhsList[-1]) )
       
    # loop over levels for postsmoothing
    for level in numpy.flipud( numpy.arange(len(systemMatrixList)-1) ):
      # update approximation with coarse grid correction
      lhsList[level] += restrictionProlongationmatrixList[level].T.dot(lhsList[level+1])
      # perform postsmoothing operations
      for forward in postSmooth:
        lhsList[level] += smooth( systemMatrixList[level], rhsList[level]-systemMatrixList[level].dot(lhsList[level]), blockListList[level], blockInverseListList[level], scheme, forward, relax )

  # in case of one level a coarse grid correction is not applied
  elif len(systemMatrixList) == 1:
    # initiate approximation vector
    lhs = numpy.zeros(rhs.shape)
    # apply all smoothing operations (pre and postsmoothing)
    for forward in preSmooth:
      lhs += smooth( systemMatrixList[0], rhs-systemMatrixList[0].dot(lhs), blockListList[0], blockInverseListList[0], scheme, forward, relax )
    for forward in postSmooth:
      lhs += smooth( systemMatrixList[0], rhs-systemMatrixList[0].dot(lhs), blockListList[0], blockInverseListList[0], scheme, forward, relax )
    lhsList = [ lhs ]

  return lhsList[0]

def fixedPoint( rhs, solveTol, maxiter, systemMatrixList, restrictionProlongationmatrixList, blockListList, blockInverseListList, scheme, preSmooth, postSmooth, relax ):
 
  lhs = numpy.zeros(rhs.shape)

  residualV = [numpy.linalg.norm(rhs,2)]

  for iteration in numpy.arange( maxiter ):

    update = VCycle( systemMatrixList, rhs, restrictionProlongationmatrixList, blockListList, blockInverseListList, scheme, preSmooth, postSmooth, relax )
    lhs += update
    rhs -= systemMatrixList[0].dot(update)

    residualV.append( numpy.linalg.norm(rhs,2) )

    print('relative residual', iteration+1, '=', residualV[-1]/residualV[0] )
    if residualV[-1]/residualV[0] < solveTol:
      break

  return lhs, residualV

def preconCG( rhs, solveTol, maxiter, systemMatrixList, restrictionProlongationmatrixList, blockListList, blockInverseListList, scheme, preSmooth, postSmooth, relax ):

  x = numpy.zeros( rhs.shape )
  z = VCycle( systemMatrixList, rhs, restrictionProlongationmatrixList, blockListList, blockInverseListList, scheme, preSmooth, postSmooth, relax )
  rho = rhs.dot(z) 
  p = z 

  residualV = [numpy.linalg.norm(rhs,2),numpy.linalg.norm(rhs,2)]

  for iteration in numpy.arange( maxiter ):

    q = systemMatrixList[0].dot(p)
    alpha = rho/p.dot(q) 
    x = x + alpha*p 
    rhs -= alpha*q
    z = VCycle( systemMatrixList, rhs, restrictionProlongationmatrixList, blockListList, blockInverseListList, scheme, preSmooth, postSmooth, relax )
    rho_old = rho 
    rho = rhs.dot(z) 
    beta = rho/rho_old 
    p = z + beta*p 
    if numpy.logical_not(numpy.isfinite(rho)) or (rho<=0):
      print( 'CG algorithm breakdown, rho =', rho )
      break 

    residualV.append( numpy.linalg.norm(rhs,2) )

    print('relative residual', iteration+1, '=', residualV[-1]/residualV[0] )
    if residualV[-1]/residualV[0] < solveTol:
      break

  return x, residualV
